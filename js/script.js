$(document).ready(function () {
    $('.slider').slick({
        slidesToShow: 4,
        dots: true,
        centerMode: false,

    });
});
$('[data-fancybox]').fancybox({
    toolbar  : false,
    smallBtn : true,
    iframe : {
        preload : false
    }
})
